FROM image-registry.openshift-image-registry.svc:5000/openshift/openjdk-11-rhel7:1.10

LABEL mantainer="Equipo administradores Cloud IBM"

USER root

ENV SURA_APP_NAME="contexto" \
    SURA_APP_TYPE="jvm" \
    SURA_APP_PORT="9416" \
    APP_NAME="contexto"

LABEL sura.docker.version="1.0" \
      sura.app.name="${SURA_APP_NAME}"

EXPOSE 9416

RUN mkdir -p /opt/app/run/${SURA_APP_NAME}/ && \
    chown daemon:root /opt/app/run/${SURA_APP_NAME}/ && \
    mkdir -p /opt/app/logs/${SURA_APP_NAME}/ && \
    chown daemon:root /opt/app/logs/${SURA_APP_NAME}/

COPY ${SURA_APP_NAME}/jvm-exec-wrapper /opt/app/aplicaciones/${SURA_APP_NAME}/
COPY ${SURA_APP_NAME}/shared/ /opt/app/shared/${SURA_APP_NAME}/
COPY ${SURA_APP_NAME}/${SURA_APP_NAME}.zip /opt/app/aplicaciones/${SURA_APP_NAME}/${SURA_APP_NAME}.zip

RUN unzip -d /opt/app/aplicaciones/${SURA_APP_NAME}/temp /opt/app/aplicaciones/${SURA_APP_NAME}/${SURA_APP_NAME}.zip && \
    rm -f /opt/app/aplicaciones/${SURA_APP_NAME}/${SURA_APP_NAME}.zip && \
    mv /opt/app/aplicaciones/${SURA_APP_NAME}/temp/${SURA_APP_NAME}/ /opt/app/aplicaciones/${SURA_APP_NAME}/ && \
    rm -rf /opt/app/aplicaciones/${SURA_APP_NAME}/temp && \
    chmod +rx /opt/app/aplicaciones/${SURA_APP_NAME}/jvm-exec-wrapper && \
    chown -R root:daemon /opt/app/shared/${SURA_APP_NAME} && \
    find /opt/app/shared/${SURA_APP_NAME} -exec chmod g+rX '{}' ';' && \
    chown -R root:daemon /opt/app/aplicaciones/${SURA_APP_NAME} && \
    chown -R root:daemon /opt/app/shared/${SURA_APP_NAME}/ && \
    find /opt/app/aplicaciones/${SURA_APP_NAME} -exec chmod g+rX '{}' ';' && \
    find /opt/app/shared/${SURA_APP_NAME}/ -exec chmod g+rX '{}' ';'

USER daemon
ENTRYPOINT ["/bin/bash", "-c", "exec /opt/app/aplicaciones/${SURA_APP_NAME}/jvm-exec-wrapper"]
